# `vstream`

`vstream` is an application which streams local video files to multiple viewers.
A __host__ selects a video file on their device to be streamed into a __room__,
which is populated by the host, and some __peers__.
Playback in the room is synchronized for all peers (like a watch party).
